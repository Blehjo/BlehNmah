﻿using System;
using Microsoft.EntityFrameworkCore;

namespace RazorPagesBlehNmah.Data
{
	public static class MigrationManager
	{
        public static WebApplication MigrateDatabase(this WebApplication webApp)
        {
            using (var scope = webApp.Services.CreateScope())
            {
                using (var appContext = scope.ServiceProvider.GetRequiredService<RazorPagesGalleryContext>())
                {
                    try
                    {
                        appContext.Database.EnsureDeleted();
                        appContext.Database.Migrate();
                    }
                    catch (Exception ex)
                    {
                        //Log errors or do anything you think it's needed
                        Console.WriteLine(ex);
                        throw;
                    }
                }
            }

            return webApp;
        }
    }
}

